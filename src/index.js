const firefox = require('selenium-webdriver/firefox');
const { Builder, By, Key, until, Options } = require('selenium-webdriver');
const { selectors, sleep } = require('./util/');
const { whatsapp } = require('./models/');
const fs = require('fs');
const bodyParser = require('body-parser');
const ini = require('ini');
const app = require('express')();
const http = require('http').Server(app);
const config = ini.parse(fs.readFileSync('./config.ini', 'utf-8'));

const db = require('./database/');

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

var io = require('socket.io')(http, {
	serveClient: false,
	pingInterval: 10000,
	pingTimeout: 5000,
	cookie: false,
	//wsEngine: 'uws'
});

let wpp = new whatsapp();
let lsFile = `${appRoot}/profile/localStorage.json`;
let driver = null;

async function wrapper() {
	global.globals.driver = driver = await new Builder()
		.forBrowser('firefox')
		.build();

	// try {
	await driver.get('https://web.whatsapp.com');
	console.info("[INFO] BROWSER INIT");

	console.info("[INFO] LOADING LOCALSTORAGE");
	if (fs.existsSync(lsFile)) {
		let lsObject = require(lsFile);

		for (k in lsObject) {
			await driver.executeScript(`window.localStorage.setItem('${k}', '${lsObject[k]}')`);
		}
	}
	console.info("[INFO] LOCALSTORAGE WAS LOADED");
	await driver.navigate().refresh();

	await initLogin();
	await saveLocalStorage();
	await injectXploit();

	http.listen(config.api.port, async function () {
		while (true) {
			let messages = await driver.executeScript('return window.WAPI._newMessagesQueue;');
			await messages.forEach(async (m) => {
				if(!m.chat.isGroup) {
					let chatResponse = await db.models.w_chats.findOrCreate({ where: { w_chat_id: m.chatId }, defaults: { is_group: 0 } });
					chatResponse = chatResponse[0];

					let contactResponse = await db.models.w_contacts.findOrCreate({
						where: {
							w_id: chatResponse.w_chat_id
						},
						defaults: {
							patient_id: null,
							professional_eid: null,
							push_name: null,
							push_avatar: null
						}
					});

					contactResponse = contactResponse[0];

					if (contactResponse.professional_eid == null && contactResponse.patient_id == null) {
						let phone = m.from.replace('55', '').replace('@c.us', '');
						let ddd = phone.substring(0, 2);
						phone = phone.substring(2);
						searchNumbers = [];
						
						searchNumbers.push(`${phone}`);
						searchNumbers.push(`${ddd}${phone}`); 
						searchNumbers.push(`55${ddd}${phone}`);

						searchNumbers.map((item) => {$iLike: item});

						let patientId = await db.models.patients.findOne({ where: { normalized_phone: searchNumbers  } });
						let professionalEid = null;

						if (patientId == null) professionalEid = await db.models.company_referring_doctor.findOne({ where: { normalized_phone: searchNumbers } });

						contactResponse = await contactResponse.update({
							professional_eid: professionalEid,
							patient_id: patientId
						});

						console.log(contactResponse);
					}
				}

				await driver.executeScript(`window.WAPI.removeMessage('${m.id}');`);
			});


			await sleep(200);
		}
	});
}

/**
 * Check if whatsapp is loggedIn!
 */
async function isLoggedIn() {
	return await driver.findElement(By.css(selectors.mainPage)).then(() => wpp.loggedIn = true).catch(() => wpp.loggedIn = false);
}

/**
 * Save current localStorage to prevent re-login
 */
async function saveLocalStorage() {
	let localStorage = await driver.executeScript('return window.localStorage;');
	fs.writeFile(lsFile, JSON.stringify(localStorage), function (err) {
		if (err) return console.log(err);
		console.log("[INFO] THE FILE WAS SAVED!");
	});

	return localStorage;
}

/**
 * Load de xploit script (wapi.js)
 */
async function injectXploit() {
	let xploit = fs.readFileSync(`${appRoot}/src/injector/wapi.js`, 'utf8');
	console.info("[INFO] XPLOIT INJECTED");
	return await driver.executeScript(xploit);
}

/**
 * Check if logged when not load qrcode
 */
async function initLogin() {
	let qrcode = null;

	do {
		console.info("[INFO] CHECKING QRCODE");
		try {
			qrcode = await driver.findElement(By.css(selectors.qrcode));
			if (qrCode != null)
				image = await qrCode.getAttribute('src');
		}
		catch (e) {
			await isLoggedIn();
			if (wpp.isLoggedIn) console.info("[INFO] LOGGED IN");
			else console.info("[INFO] NOT LOGGED");
		}

		await sleep(3000);
	} while (!wpp.loggedIn);
}

exports.default = wrapper;