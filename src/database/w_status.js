/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('w_status', {
    status: {
      type: DataTypes.STRING(20),
      allowNull: false
    },
    last_update: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'w_status'
  });
};
