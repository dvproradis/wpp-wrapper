/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('company_referring_doctor', {
    company_eid: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'company',
        key: 'eid'
      }
    },
    eid: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'entities',
        key: 'eid'
      }
    },
    name: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    tel: {
      type: DataTypes.STRING(15),
      allowNull: true
    },
    cel: {
      type: DataTypes.STRING(15),
      allowNull: true
    },
    email: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    CEP: {
      type: DataTypes.STRING(9),
      allowNull: true
    },
    street: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    number: {
      type: DataTypes.STRING(128),
      allowNull: true
    },
    comp: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    neighborhood: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    city: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    uf: {
      type: DataTypes.STRING(2),
      allowNull: true
    },
    crm1: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    crm2: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    specialty1: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    specialty2: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    hash: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    active: {
      type: DataTypes.INTEGER(4),
      allowNull: true,
      defaultValue: '0'
    },
    notify: {
      type: DataTypes.INTEGER(4),
      allowNull: true,
      defaultValue: '1'
    },
    birthdate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    obs: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    name_on_guide: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    cnpj: {
      type: DataTypes.STRING(45),
      allowNull: true,
      defaultValue: '99999999999999'
    },
    normalized_phone: {
      type: DataTypes.STRING(18),
      allowNull: true
    }
  }, {
    tableName: 'company_referring_doctor'
  });
};
