/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('w_users', {
    userId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    username: {
      type: DataTypes.STRING(80),
      allowNull: true
    },
    complete_name: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    avatar: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    email: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    clinic_name: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    old_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    }
  }, {
    tableName: 'w_users'
  });
};
