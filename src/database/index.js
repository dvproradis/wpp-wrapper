const fs = require('fs');
const ini = require('ini');
const config = ini.parse(fs.readFileSync('./config.ini', 'utf-8'));
const Sequelize = require('sequelize');

const db = new Sequelize(config.database.base, config.database.user, config.database.pass, {
	host: config.database.host,
	define: {
    charset: 'utf8mb4',
    collate: 'utf8mb4_general_ci', 
    timestamps: false
  },
  dialect: 'mysql',
  operatorsAliases: false,

  pool: {
    max: config.database.connectionLimit,
    min: 0,
    acquire: 30000,
    idle: 10000
  },
});

db.import('w_messages', require('./w_messages.js'));
db.import('w_chats', require('./w_chats.js'));
db.import('w_contacts', require('./w_contacts.js'));
db.import('w_status', require('./w_status.js'));
db.import('w_users', require('./w_users.js'));
db.import('patients', require('./patients.js'));
db.import('company_referring_doctor', require('./company_referring_doctor.js'));

module.exports = db;