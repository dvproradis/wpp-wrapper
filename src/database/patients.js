/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('patients', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    sex: {
      type: DataTypes.ENUM('F','M'),
      allowNull: true
    },
    birthdate: {
      type: DataTypes.DATEONLY,
      allowNull: false
    },
    nationality: {
      type: DataTypes.STRING(3),
      allowNull: false,
      defaultValue: '010'
    },
    skin_color: {
      type: DataTypes.STRING(2),
      allowNull: false,
      defaultValue: '99'
    },
    rg: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    cpf: {
      type: DataTypes.STRING(14),
      allowNull: true
    },
    uf: {
      type: DataTypes.STRING(2),
      allowNull: true
    },
    city: {
      type: DataTypes.STRING(125),
      allowNull: true
    },
    address_area_code: {
      type: DataTypes.STRING(3),
      allowNull: false,
      defaultValue: '081'
    },
    address: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    address_number: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    complement: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    cep: {
      type: DataTypes.STRING(9),
      allowNull: true
    },
    lat: {
      type: DataTypes.FLOAT,
      allowNull: false
    },
    lng: {
      type: DataTypes.FLOAT,
      allowNull: false
    },
    phone: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    cel: {
      type: DataTypes.STRING(17),
      allowNull: true
    },
    created_time: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    updated_time: {
      type: DataTypes.DATE,
      allowNull: true
    },
    user_eid: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'users',
        key: 'eid'
      }
    },
    hash_name: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    hash_patient: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    enabled: {
      type: DataTypes.INTEGER(4),
      allowNull: false,
      defaultValue: '1'
    },
    obs: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    mother_name: {
      type: DataTypes.STRING(155),
      allowNull: true
    },
    neighborhood: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    email: {
      type: DataTypes.STRING(155),
      allowNull: true
    },
    profession: {
      type: DataTypes.STRING(155),
      allowNull: true
    },
    responsible_name: {
      type: DataTypes.STRING(155),
      allowNull: true
    },
    responsible_birthdate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    responsible_profession: {
      type: DataTypes.STRING(155),
      allowNull: true
    },
    company_eid: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '2',
      references: {
        model: 'company',
        key: 'eid'
      }
    },
    id_card_number: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    external_id: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    external_label: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    responsible_document: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    responsible_relationship: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    custom_fields: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    cid10: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    insurance: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    insurance_registry: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    escolaridade: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    protocol_access: {
      type: DataTypes.STRING(8),
      allowNull: true
    },
    protocol: {
      type: DataTypes.STRING(8),
      allowNull: true
    },
    password: {
      type: DataTypes.STRING(8),
      allowNull: true
    },
    normalized_phone: {
      type: DataTypes.STRING(20),
      allowNull: true
    }
  }, {
    tableName: 'patients'
  });
};
