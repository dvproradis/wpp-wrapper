/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('w_chats', {
    chat_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    w_chat_id: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    is_group: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '0'
    }
  }, {
    tableName: 'w_chats'
  });
};
