/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('w_messages', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    chat_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'w_chats',
        key: 'chat_id'
      }
    },
    contact_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'w_contacts',
        key: 'id'
      }
    },
    quoted_message_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    timestamp: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    type: {
      type: DataTypes.STRING(30),
      allowNull: false,
      defaultValue: 'text'
    },
    mime: {
      type: DataTypes.STRING(25),
      allowNull: true
    },
    is_me: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: '0'
    },
    is_read: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: '0'
    },
    content: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    w_userId: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    is_sent: {
      type: DataTypes.INTEGER(4),
      allowNull: true
    }
  }, {
    tableName: 'w_messages'
  });
};
