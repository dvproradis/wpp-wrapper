/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('w_contacts', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    w_id: {
      type: DataTypes.STRING(20),
      allowNull: false
    },
    patient_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    professional_eid: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    push_name: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    push_avatar: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    tableName: 'w_contacts'
  });
};
