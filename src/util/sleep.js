/**
 * Delay
 * @param {int} time Time in ms
 */
exports.default = (time) => new Promise((resolve) => setTimeout(resolve, time));