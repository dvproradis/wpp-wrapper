exports.default = {
	/**
	 * Selector to find qrcode
	 */
	qrcode: 'img[alt="Scan me!"]',
	mainPage: '.app.two'
}