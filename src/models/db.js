const config = ini.parse(fs.readFileSync('../../config.ini', 'utf-8'));
const mysql = require('mysql');

var pool = mysql.createPool({
	connectionLimit: config.database.connectionLimit,
	host: config.database.host,
	user: config.database.user,
	password: config.database.pass,
	database: config.database.base,
	charset: config.database.charset,
	queryFormat: function (query, values) {
		if (!values) return query;
		return query.replace(/\:(\w+)/g, function (txt, key) {
			if (values.hasOwnProperty(key)) {
				return this.escape(values[key]);
			}
			return txt;
		}.bind(this));
	}
});

exports.default = {
	/**
	 * Esta função é necessária para zerar todas as mensagens que não foram emitidas para o cliente evitando assim o risco de mensagens duplicadas
	 */
	initDb() {
		return new Promise((resolve, reject) => {
			pool.getConnection((err, db) => {
				db.query("SET character_set_client='utf8mb4'; SET character_set_connection='utf8mb4'; SET character_set_results='utf8mb4'; SET NAMES utf8mb4;", {}, () => {
					db.query("UPDATE w_messages SET is_sent = 1 WHERE is_read = 0 and is_me = 0 and is_sent is null", {}, (err, result) => {
						db.release();
						if (err) { reject(err); }
						resolve(result)
					})
				})
			})
		})
	},

	/**
	 * Retorna o usuário logado ou cadastra se necessário
	 */
	getUser(data) {
		return new Promise((resolve, reject) => {
			pool.getConnection((err, db) => {
				db.query("SELECT * FROM w_users wu WHERE wu.username = :username", { username: data.username }, (err, result) => {
					if (err) {
						db.release(); return reject(err);
					} else {
						if (result.length == 0) {
							if (data.username != null)
								db.query("INSERT INTO w_users (username, email, clinic_name, complete_name) values (:username, :email, :clinicName, :completeName)", {
									username: data.username,
									email: data.email,
									clinicName: data.company_name,
									completeName: data.name
								}, (err, result) => {
									db.commit();

									if (err) {
										db.release(); return reject(err);
									} else {
										db.query("SELECT * FROM w_users wu WHERE wu.userId = :id", { id: result.insertId }, (err, result) => {
											db.release();
											resolve(result[0]);
										});
									}
								});
							else {
								db.release();
								resolve(null)
							}
						}
						else {
							db.release();
							resolve(result[0]);
						}
					}
				});
			});
		})
	},

	/**
	 * Retorna todos os usuários autenticados para usar o sistema
	 */
	getUsers() {
		return new Promise((resolve, reject) => {
			pool.getConnection((err, db) => {
				db.query("SELECT * FROM w_users wu", (err, result) => {
					db.release();
					if (err) { reject(err); }
					resolve(result)
				});
			})
		})
	},

	/**
	 * Retorna os ultimos {n} chats recentes
	 */
	getRecentChats(chatsCount) {
		return new Promise((resolve, reject) => {
			pool.getConnection((err, db) => {
				db.query('SELECT chat_id as last_message FROM w_messages GROUP BY chat_id ORDER BY max(timestamp) DESC LIMIT 0, :chatCount', {chatCount: chatsCount}, (err, result) => {
					if (err) { db.release(); return reject(err); }
					db.query(
						`
						SELECT 
							c.chat_id, 
							c.is_group, 
							c.w_chat_id, 
							co.id, 
							co.patient_id, 
							co.professional_eid, 
							co.push_name,
							co.push_avatar,
							p.name as patient_name,
							medic.name as medic_name
						FROM w_chats c 
						INNER JOIN
							w_contacts co on co.w_id = c.w_chat_id
						LEFT JOIN
							patients p on p.id = co.patient_id
						LEFT JOIN
							company_referring_doctor medic on medic.eid = co.professional_eid
						WHERE c.chat_id in (:chatsIds)
				`, { chatsIds: result.map((a) => a) }, (err, result) => {
							if (err) { db.release(); return reject(err); }

							let a = [];

							result.forEach(item => {
								a.push(new Promise((resolve, reject) => db.query("SELECT *, UNIX_TIMESTAMP(m.timestamp) as timestamp from w_messages m WHERE m.chat_id = :chatId ORDER BY m.timestamp DESC LIMIT 20", { chatId: item.chat_id }, (err, messages) => {
									item.messages = messages;
									// item.unreadCount = messages.filter(m => m.is_read == 0 && m.w_userId == null).length;
									let lastMessage = _.maxBy(messages, (m => m.timestamp));
									item.lastMessage = lastMessage ? lastMessage : null;

									resolve(messages)
									messages = null;
								})))
							});

							Promise.all(a).then(() => {
								chats = _.orderBy(result, (chat) => {
									return chat.lastMessage && !(chat.lastMessage.is_me == 1 && chat.lastMessage.w_userId == null) ? chat.lastMessage.timestamp : 0
								}, 'desc');

								db.release();
								resolve(chats);
								chats = null;
							});
						}
					)
				})
			})
		})
	},

	/**
	 * Adiciona uma mensagem ao banco de dados para ser enviando
	 */
	addMessage(message, api) {
		return new Promise((resolve, reject) => {
			pool.getConnection((err, db) => {
				let sql = mysql.format('insert into w_messages (chat_id, contact_id, timestamp, content, type, mime, quoted_message_id, is_me, is_read, w_userId) values (?, ?, CURRENT_TIMESTAMP, ?, ?, ?, ?, ?, ?, ?)', [message.chat_id, message.contact_id, message.content, message.type, message.mime, null, message.is_me, message.is_read, message.user_id]);

				let queryAnalyzer = '';

				if (api) queryAnalyzer = "SELECT * FROM w_messages where content = :content AND chat_id = :chatId AND timestamp >= NOW() - INTERVAL 24 HOUR";
				else queryAnalyzer = "SELECT * FROM w_messages where content = :content AND chat_id = :chatId AND timestamp >= NOW() - INTERVAL 5 MINUTE"

				db.query(queryAnalyzer, { chatId: message.chat_id, content: message.content }, (err, result) => {
					if (result.length == 0) {
						db.query(sql, {}, (err, result) => {
							db.query("SELECT *, UNIX_TIMESTAMP(timestamp) as timestamp FROM w_messages where id = :id", { id: result.insertId }, (err, result) => {
								db.release();
								if (err) reject(err);
								else resolve(result);
							})
						})
					} else {
						db.release();
						resolve(null);
					}
				});
			})
		})
	},

	/**
	 * Atualizar chat pata lido.
	 */
	setChatRead(data) {
		return new Promise((resolve, reject) => {
			pool.getConnection((err, db) => {
				db.query("UPDATE w_messages set is_read = 1 where chat_id = :chatId", { chatId: data.chat_id }, (err, result) => {
					db.release();
					if (err) reject(err);
					else resolve(result);
				})
			})
		})
	},

	/**
	 * Retorna novas msgs recebidas do banco de dados
	 */
	getNewMessages() {
		return new Promise((resolve, reject) => {
			pool.getConnection((err, db) => {
				db.query("SELECT *, UNIX_TIMESTAMP(m.timestamp) as timestamp from w_messages m WHERE m.is_read = 0 and m.is_me = 0 and is_sent is null ORDER BY m.timestamp DESC", {}, (err, result) => {
					db.release()
					if (err) reject(err);
					else resolve(result);
				})
			})
		})
	},

	/**
	 * Verifica se o número passado pertence à um médico ou paciente
	 */
	findPossibleId(data) {
		return new Promise((resolve, reject) => {
			pool.getConnection((err, db) => {
				let phone = data.toString().replace('55', '');
				let ddd = phone.substring(0, 2);

				phone = phone.substring(2);

				let sql = mysql.format("(SELECT id as patient_id, NULL as professional_eid FROM patients where normalized_phone like ? or normalized_phone like ? or normalized_phone like ?) UNION ALL (SELECT NULL as patient_id, eid as professional_eid FROM company_referring_doctor where normalized_phone like ? or normalized_phone like ? or normalized_phone like ?)", [`%${phone}%`, `%${ddd}${phone}%`, `%55${ddd}${phone}%`, `%${phone}%`, `%${ddd}${phone}%`, `%55${ddd}${phone}%`]);

				console.log("ATENÇÃO BUSCANDO PACIENTE")
				console.log(sql)

				db.query(sql, {}, (err, result) => {
					db.release();
					if (err) reject(err);
					else {
						if (result)
							resolve(result[0]);
						else
							resolve({
								professional_eid: null,
								patient_id: null
							})
					}
				})
			})
		})
	},

	/**
	 * Retorna um chat ou cria
	 */
	getChat(data) {
		return new Promise((resolve, reject) => {

			let phone = data.toString().replace('55', '');
			let ddd = phone.substring(0, 2);

			phone = phone.substring(2);
			data = `${ddd}${phone}`.length == 8 ? `55${ddd}9${phone}@c.us` : `55${ddd}${phone}@c.us`;

			pool.getConnection((err, db) => {
				db.query("CALL ws_chat(0, :chatId, 0)", {
					chatId: data,
				}, (err, result) => {
					db.release();
					if (err) { reject(err); }
					else resolve(result[0][0]);
				})
			})
		})
	},

	/**
	 * Retorna somente os IDS dos chats
	 */
	getAllChatsIds() {
		return new Promise((resolve, reject) => {
			pool.getConnection((err, db) => {
				db.query("SELECT chat_id FROM w_chats", {}, (err, result) => {
					db.release();
					if (err) { reject(err); }
					else resolve(result);
				})
			})
		})
	},

	/**
	 * Retorna um contato ou cria
	 */
	getContact(data) {
		return new Promise((resolve, reject) => {
			pool.getConnection((err, db) => {
				this.findPossibleId(data).then((possible) => {
					db.query("CALL ws_chat_contact(0, :chatId, :patientId, :professional_eid, null, null)", {
						chatId: `${data}@c.us`,
						patientId: data.patient_id,
						professional_eid: data.professional_eid
					}, (err, result) => {
						db.release();
						if (err) reject(err);
						else resolve(result[0][0]);
					})
				})
			})
		})
	},

	/**
	 * Verifica a conexão do whatsapp, se a conexão não estiver ok a cada 1 minuto.. dropa a conexão
	 */
	getStatus() {
		return new Promise((resolve, reject) => {
			pool.getConnection((err, db) => {
				db.query("SELECT * from w_status", {}, (err, result) => {
					db.release();
					if (err) { reject(err); }
					else {
						data = result[0];
						isOk = !(new Date - new Date(data.last_update) > MIN)
						resolve({
							isOk: isOk,
							status: isOk ? data.status : 'NotLoggedIn',
							lastUpdate: data.last_update
						})
					}
				})
			})
		})
	}
}